#   Prueba Back Nisum

Este microservicio hecho en Flask permite traer data de posts desde un endpoint, además regresa la data filtrado por id de usuario.
Además permite generar un topic de Apache Kafka, generar un producer y un consumer a este topic

### Requisitos 📋

Docker
Docker-compose

### Deployment 📦

* Construir imagen de flask: `docker-compose -f local.yml build`
* Inicializar el archivo local.yml: `docker-compose -f local.yml up`
* Generar un topic: `docker-compose -f local.yml run --rm flask python kafka_admin.py`
* Generar un producer: `docker-compose -f local.yml run --rm flask python kafka_producer.py`
* Generar un consumer: `docker-compose -f local.yml run --rm flask python kafka_consumer.py`