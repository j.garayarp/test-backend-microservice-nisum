# FLask
from flask import Flask, make_response

# Tools
import requests
from requests.exceptions import HTTPError


app = Flask(__name__)


@app.route('/posts/<id_user>', methods=['GET'])
def get_posts_from_user(id_user):
    try:
        response = requests.get('https://jsonplaceholder.typicode.com/posts')
        data = response.json()
    except HTTPError as http_err:
        make_response(f'Un error HTTP ocurrió: {http_err}', 500)
    except ConnectionError as http_con:
        make_response(f'Un error de conexión ocurrió: {http_con}', 500)
    except TimeoutError as http_timeout:
        make_response(f'Tiempo de espera para respuesta terminado: {http_timeout}', 504)
    except Exception as err:
        make_response(f'Ocurrió un error desconocido: {err}', 500)
    
    try:
        id_user = int(id_user)
    except Exception as err:
        return make_response({'msg': 'Ingrese un valor entero para id_user', 'error': f'{err}'}, 400)
    
    posts_from_user = list(filter(lambda x: x['userId'] == id_user, data))
    return make_response({'data': posts_from_user, 'msg': 'Consulta exitosa'}, 200)


if __name__ == '__main__':
    app.run(port=5000, debug=True)
