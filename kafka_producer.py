# Apache Kafka
from kafka import KafkaProducer

# Utilities
import json
import time


producer = KafkaProducer(
    bootstrap_servers=["kafka:9092"],
    value_serializer=lambda data: json.dumps(data).encode('utf-8')
)

if __name__ == '__main__':
    while True:
        msg = {
            'userId': 1,
            'Title': 'Test 1',
            'Body': 'This is a test'
        }
        print('Sending message')
        producer.send("my-topic", msg)
        time.sleep(5)