from kafka import KafkaConsumer
import json


if __name__ == '__main__':
    consumer = KafkaConsumer(
        'my-topic',
        bootstrap_servers=['kafka:9092'],
        auto_offset_reset='earliest'
    )
    print ("Starting consumer")
    for msg in consumer:
        print('Receiving new message')
        print(json.loads(msg.value))